from pickletools import float8
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
#import pathlib as Path
#map_path = Path("gl-latlong-1km-landcover.bsq")

map_numpy = np.memmap('gl-latlong-1km-landcover.bsq', shape=(21600, 43200,), dtype=np.int8)
#print(map_numpy[6300,25000])
def is_this_land(WE:float8,SN:float8,map_transform):
    '''x and y arecoordinates from teh globe system'''
    #48.95 9.13
    const_transform = 180/map_transform.shape[0]
    y_new = (90 - SN)/const_transform
    x_new = (WE + 180)/const_transform
    print(y_new, x_new)
    try:
        if map_transform[int(y_new), int(x_new)]>0:
            ans=('Land')
        else:
            ans=('Water') 
        return ans
    except:
        print('coordinates out of the limit, try another set')
        pass
 
WE=71 #WE
SN=53 #SN


print (is_this_land(WE,SN, map_numpy))

# maryland_colors = np.array([( 68,  79, 137),
#                                 (  1, 100,   0),
#                                 (  1, 130,   0),
#                                 (151, 191,  71),
#                                 (  2, 220,   0),
#                                 (  0, 255,   0),
#                                 (146, 174,  47),
#                                 (220, 206,   0),
#                                 (255, 173,   0),
#                                 (255, 251, 195),
#                                 (140,  72,   9),
#                                 (247, 165, 255),
#                                 (255, 199, 174),
#                                 (  0, 255, 255),]) / 255
# maryland_cmap = colors.ListedColormap(maryland_colors)

# # fig = plt.figure()
# # im1 = plt.imshow(map_numpy[::50,::50], cmap=maryland_cmap)
# # plt.show()

# import pandas as pd

# erq_file = np.loadtxt('events_4.5.txt', delimiter=';',dtype=str,)
# #df = pd.read_table("events_4.5.txt", delimiter=" ")
# print(erq_file[1,:])

# # new = map_numpy
# # map_numpy